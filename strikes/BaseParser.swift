//
//  BaseParser.swift
//  strikes
//
//  Created by Ahmed Zaytoun on 6/25/16.
//  Copyright © 2016 spier. All rights reserved.
//

import UIKit

class BaseParser: NSObject {

    
    func isValidJsonObject(jsonObject : AnyObject?) -> Bool
    {
        //check jsond object is not nill or is not NSNull object
        return (jsonObject != nil && !(jsonObject is NSNull)) && (jsonObject is NSDictionary || jsonObject is NSArray || jsonObject is NSString || jsonObject is NSNumber)
    }
    
    /**
     Get Number Data from json dictionary
     */
    func getNumberDataFromObject(jsonObject : AnyObject!, forKey key : String) -> NSNumber!
    {
        
        //check if json data is valid, if not return nil
        if !self.isValidJsonObject(jsonObject) || !(jsonObject is NSDictionary)
        {
            return nil
        }
        
        // get data
        let dataObject : AnyObject? = (jsonObject as! NSDictionary).objectForKey(key)
        
        //check if data is in required type
        if dataObject != nil && (dataObject is NSNumber)
        {
            //return data
            return dataObject as! NSNumber
        }
        
        //otherwise return nil
        return nil
        
    }
    
    /**
     Get String Data from json dictionary
     */
    func getStringDataFromObject(jsonObject : AnyObject!, forKey key : String) -> String!
    {
        
        //check if json data is valid, if not return nil
        if !self.isValidJsonObject(jsonObject) || !(jsonObject is NSDictionary)
        {
            return nil
        }
        
        // get data
        let dataObject : AnyObject? = (jsonObject as! NSDictionary).objectForKey(key)
        
        //check if data is in required type
        if dataObject != nil && dataObject is String
        {
            //return data
            return dataObject as! String
        }
        
        //otherwise return nil
        return nil
        
    }
    
}
