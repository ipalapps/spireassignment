//
//  ViewController.swift
//  streaks
//
//  Created by Ahmed Zaytoun on 6/25/16.
//  Copyright © 2016 spier. All rights reserved.
//

import UIKit

enum CellType {
    
    case PHOTO
    case LOCATION
    case OTHER
}

struct CellData
{
    var cellIdentifier : String!
    var cellType : CellType = .OTHER
    var data : AnyObject?
}

struct SectionData
{
    var header : String!
    var sectionCells : Array<CellData>!
}

class ViewController: UIViewController, UITableViewDelegate,UITableViewDataSource, DataLoadingOperationDelegate {

    var dataLoadOperation : DataLoadingOpertaion = DataLoadingOpertaion()
    
    var imageDownloader : ImageServiceManager = ImageServiceManager.getImageDownloaderManagerInstance()
    
    var opertionQueue = NSOperationQueue()
    
    var daysList : Array<NSDate> = Array<NSDate>()
    var dataCache : Dictionary<String,SectionData> = Dictionary<String,SectionData>()
    
    @IBOutlet weak var streaksTable : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
        self.title = "Streaks List"
        
        self.opertionQueue.maxConcurrentOperationCount = 2
        
        self.dataLoadOperation.delegate = self
        
        let newDate = DateUtils.getDateTimeZero(NSDate())
        
        self.loadStreaksDataForDate(newDate)
    
        daysList.append(newDate)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(didImageLoaded), name: ImageServiceManagerConstants.DID_FINISH_DOWNLOAD, object: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func didLoadDataWithResult(result: DayStreakEntity!) {
    
        var sectionData = SectionData()
        
        let dateString = DateUtils.getStringDate(result.date, forSystemFormat: SystemFormats.LONG_DATE_ONLY)
        
        sectionData.header = dateString
        
        let data = result
        
        var cellsArray = Array<CellData>()
        
        for item in data.streaks
        {
            var cellData = CellData()
            
            if item.photo != nil
            {
                cellData.cellIdentifier = "PhotoCell"
                cellData.data = item
                cellData.cellType = CellType.PHOTO
                
                cellsArray.append(cellData)
            }
            else if item.location != nil
            {
                cellData.cellIdentifier = "MapCell"
                cellData.data = item
                cellData.cellType = CellType.LOCATION
                
                cellsArray.append(cellData)
                
            }   
        }
        
        sectionData.sectionCells = cellsArray
        
        self.dataCache[dateString] = sectionData
        
        self.streaksTable.performSelectorOnMainThread(#selector(self.streaksTable.reloadData), withObject: nil, waitUntilDone: true)
    }
    
    func failedToLoadDataWithError(error: NSError?) {
        
    }

    func loadStreaksDataForDate(date : NSDate)
    {
        let operation  = NSBlockOperation(block: {
            self.dataLoadOperation.loadDataForDay(date)
        })
        
        self.opertionQueue.addOperation(operation)
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return self.daysList.count
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let day = self.daysList[section]
        
        let dayKey = DateUtils.getStringDate(day, forSystemFormat: SystemFormats.LONG_DATE_ONLY)
        
        let sectionData = self.dataCache[dayKey]
        
        return sectionData != nil && sectionData?
            .sectionCells != nil ?  sectionData!.sectionCells.count : 0
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let day = self.daysList[section]
        
        let dayKey = DateUtils.getStringDate(day, forSystemFormat: SystemFormats.LONG_DATE_ONLY)
        
        let sectionData = self.dataCache[dayKey]
        
        return sectionData?.header
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let day = self.daysList[indexPath.section]
        
        let dayKey = DateUtils.getStringDate(day, forSystemFormat: SystemFormats.LONG_DATE_ONLY)
        
        let sectionData = self.dataCache[dayKey]
        
        if let cellData = sectionData?.sectionCells[indexPath.row]
        {
            switch cellData.cellType
            {
            case .LOCATION :
                
                let locationCell = tableView.dequeueReusableCellWithIdentifier(cellData.cellIdentifier) as! MapTableViewCell
                
                if cellData.data != nil && cellData.data is StreakEntity
                {
                    
                    let data = cellData.data as! StreakEntity
                    
                    locationCell.typeLabel.text = data.type
                    locationCell.durationLabel.text = "\(data.duration)"
                    locationCell.startTimeLabel.text = "\(data.startTime)"
                
                }
                
                return locationCell
                
            case CellType.PHOTO:
                
                let photoCell = tableView.dequeueReusableCellWithIdentifier(cellData.cellIdentifier) as! PhotoTableViewCell
                
                if cellData.data != nil && cellData.data is StreakEntity
                {
                    
                    let data = cellData.data as! StreakEntity
                    
                    photoCell.typeLabel.text = data.type
                    photoCell.durationLabel.text = "\(data.duration)"
                    photoCell.startTimeLabel.text = "\(data.startTime)"
                
                    if data.photo != nil
                    {
                        let imageModel = self.imageDownloader.getImageForIdentifier(data.photo!.url, forURL: NSURL(string:data.photo!.url), fromCache: ImageCaches.ALL)
                        
                        if imageModel != nil && imageModel.status == ImageStatus.DOWNLOADED && imageModel.image != nil
                        {
                            photoCell.photoImageView.image = imageModel.image
                        }
                        else{
                            photoCell.photoImageView.image = nil
                        }
                        
                    }
                    
                    
                }
                
                return photoCell
                
            default:
                break;
            
            }
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell")
        
        return cell!
    }
    
    func didImageLoaded()
    {
        self.streaksTable.performSelectorOnMainThread(#selector(self.streaksTable.reloadData), withObject: nil, waitUntilDone: true)
    }
    
}

