

import UIKit

import AFNetworking

struct SystemResponse {
    var data : NSData!
    var httpResponse : NSHTTPURLResponse
}

protocol InternetConnectionDelegate
{
    func internetConnectionDisconnected(fromService service : DataServices)
    func internetConnectionReconnected(fromService service : DataServices)
}

protocol RequestManagerDelegate : NSObjectProtocol
{

    func didLoadDataWithResponse(response : NSURLResponse?, dataResponse : AnyObject?, forService service : DataServices)
    
    func failedToLoadDataWithError(response : NSURLResponse? , error : NSError?, forService service : DataServices)    
}

class RequestManager: NSObject, NSURLSessionDelegate {
   
    var internetConnectionDelegate : InternetConnectionDelegate!
    
    weak var delegate : RequestManagerDelegate!
    
    func getDataForService(service : DataServices, withBody body:NSData! , withParam param : String! , withOptions options : Dictionary<String,String>!)
        
    {

        if body != nil && body.length > 0
        {
            Logger.debug("Body:\n\(String(NSString(data: body, encoding: NSUTF8StringEncoding)!))")
        }
        
        let request : NSURLRequest = RequestUtil.getRrequestForService(service, body: body, param: param
            , options: options)
        
        let configiration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        let manager = AFURLSessionManager(sessionConfiguration: configiration)
        

        let dataTask = manager.dataTaskWithRequest(request, completionHandler:
            {
                (urlResponse : NSURLResponse?, dataResponse : AnyObject?  , resError : NSError?) in
                
                if resError != nil
                {
                    
                    if self.delegate != nil
                    {
                        self.delegate.failedToLoadDataWithError(urlResponse, error: resError,forService: service)
                    }
                    
                }
                else {
                    
                    if self.delegate != nil
                    {
                        self.delegate.didLoadDataWithResponse(urlResponse, dataResponse: dataResponse,forService: service)
                    }
                    
                }
                
        })
        
        dataTask.resume()
    }
    

    
}
