//
//  PhotoDataParser.swift
//  strikes
//
//  Created by Ahmed Zaytoun on 6/25/16.
//  Copyright © 2016 spier. All rights reserved.
//

import UIKit

class PhotoDataParser: BaseParser {

    func paserPhotosList(jsonObject : AnyObject?) -> Array<PhotoEntity>!
    {
        if self.isValidJsonObject(jsonObject) && jsonObject is NSArray
        {
            var dataArray = Array<PhotoEntity>()
            
            for item in jsonObject as! NSArray
            {
                let photo = self.parsePhotoData(item)
                
                if photo != nil
                {
                    dataArray.append(photo)
                }
            }
            
            return dataArray
        }
        
        return nil
    }
    
    func parsePhotoData(jsonObject : AnyObject?) -> PhotoEntity!
    {
        if self.isValidJsonObject(jsonObject) && jsonObject is NSDictionary
        {
            let photo = PhotoEntity()
            
            photo.url = self.getStringDataFromObject(jsonObject, forKey: "url")
            photo.taken = self.getNumberDataFromObject(jsonObject, forKey: "taken_at")
            
            return photo
        }
   
        return nil
    }
    
    
}
