//
//  LocationsParser.swift
//  strikes
//
//  Created by Ahmed Zaytoun on 6/25/16.
//  Copyright © 2016 spier. All rights reserved.
//

import UIKit

class LocationsParser: BaseParser {

    func paserLocationsList(jsonObject : AnyObject?) -> Array<LocationEntity>!
    {
        if self.isValidJsonObject(jsonObject) && jsonObject is NSArray
        {
            var dataArray = Array<LocationEntity>()
            
            for item in jsonObject as! NSArray
            {
                let location = self.parseLocationData(item)
                
                if location != nil
                {
                    dataArray.append(location)
                }
            }
            
            return dataArray
        }
        
        return nil
    }
    
    func parseLocationData(jsonObject : AnyObject?) -> LocationEntity!
    {
        if self.isValidJsonObject(jsonObject) && jsonObject is NSDictionary
        {
            let location = LocationEntity()
            
            location.arrivedAt = self.getNumberDataFromObject(jsonObject, forKey: "arrived_at")
            location.latitude = self.getNumberDataFromObject(jsonObject, forKey: "latitude")
            location.longitude = self.getNumberDataFromObject(jsonObject, forKey: "longitude")
            
            return location
        }
        
        return nil
    }
    
}
