//
//  ServiceManager.swift
//  fadfid
//
//  Created by Ahmed Zaytoun on 3/19/16.
//  Copyright © 2016 iPalApps. All rights reserved.
//

import UIKit

protocol ServiceManagerDelegate : NSObjectProtocol
{
    func didLoadData(data : AnyObject?, withResponse response : NSURLResponse?)
    func failedToLoadDataWithError(error : NSError?, withResponse response : NSURLResponse?)

}

class ServiceManager: NSObject , RequestManagerDelegate{
    
    private var requestManager : RequestManager!
    
    weak var delegate : ServiceManagerDelegate!
    
    override init() {
        
        super.init()
        
        self.requestManager = RequestManager()
        
        self.requestManager.delegate = self
        
    }
    
    func getStreaksForDay(day : NSNumber)
    {
        self.requestManager.getDataForService(DataServices.STREAKS, withBody: nil, withParam: day.stringValue, withOptions: RequestUtil.getJsonRequestOptions())
    }
    
    func getPhotosForDay(day : NSNumber)
    {
        self.requestManager.getDataForService(DataServices.PHOTOS, withBody: nil, withParam: day.stringValue, withOptions: RequestUtil.getJsonRequestOptions())
    }
    
    func getLocationsForDay(day : NSNumber)
    {
        self.requestManager.getDataForService(DataServices.LOCATIONS, withBody: nil, withParam: day.stringValue, withOptions: RequestUtil.getJsonRequestOptions())
    }
    
    func didLoadDataWithResponse(response: NSURLResponse?, dataResponse: AnyObject?, forService service: DataServices) {
        
        Logger.debug("\(response) \(dataResponse)")
        
        if self.delegate != nil
        {
            self.delegate.didLoadData(dataResponse, withResponse: response)
        }
    }
    
    func failedToLoadDataWithError(response: NSURLResponse?, error: NSError?, forService service: DataServices) {
      
        Logger.debug("\(response) \(error)")
        
        if self.delegate != nil
        {
            self.delegate.failedToLoadDataWithError(error, withResponse: response)
        }
    }
    
}
