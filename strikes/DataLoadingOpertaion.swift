//
//  DataLoadingOpertaion.swift
//  strikes
//
//  Created by Ahmed Zaytoun on 6/25/16.
//  Copyright © 2016 spier. All rights reserved.
//

import UIKit

protocol DataLoadingOperationDelegate : NSObjectProtocol {
    
    func didLoadDataWithResult(result : DayStreakEntity!)
    func failedToLoadDataWithError(error : NSError?)
}

class DataLoadingOpertaion: NSObject,LocationsOperationDelegate,PhotosOperationDelegate,StreakOperationDelegate {

    var streakOperation : StreakOperations!
    var photoOperation : PhotosOperation!
    var locationOperation : LocationsOperation!
    
    var streakDataList : Array<StreakEntity>!
    
    weak var delegate : DataLoadingOperationDelegate!
    
    private var selectedDay : NSNumber!

    private var dayStreakDataModel : DayStreakEntity!
    
    override init() {
        
        super.init()
        
        self.streakOperation = StreakOperations()
        self.photoOperation = PhotosOperation()
        self.locationOperation = LocationsOperation()
        
        self.streakOperation.delegate = self
        self.photoOperation.delegate = self
        self.locationOperation.delegate = self
        
    }
    
    func loadDataForDay(dayDate : NSDate)
    {
        
        self.dayStreakDataModel = DayStreakEntity()
        
        let day = NSNumber(double:dayDate.timeIntervalSince1970)
        
        self.selectedDay = day
        
        self.dayStreakDataModel.day = dayDate.timeIntervalSince1970
        
        self.dayStreakDataModel.date = dayDate
        
        self.dayStreakDataModel.stringDate = DateUtils.getStringDate(dayDate, forSystemFormat: SystemFormats.LONG_DATE_ONLY)
        
        self.streakOperation.loadStreaksDataForDay(day)
        
    }
    
    func didLoadStrakDataWithData(dataList: Array<StreakEntity>!) {
        
        self.streakDataList = dataList
        
        self.photoOperation.loadPhotosDataForDay(self.selectedDay)
        
    }
    
    func didLoadPhotosDataWithData(dataList: Array<PhotoEntity>!) {
     
        if self.streakDataList != nil && !streakDataList.isEmpty
        {
            for photo in dataList
            {
                for streak in self.streakDataList
                {
                    if photo.taken.integerValue >= streak.start.integerValue && photo.taken.integerValue  <= streak.end.integerValue
                    {
                        streak.photo = photo
                        
                        break
                    }
                }
            }
            
            self.locationOperation.loadPhotosDataForDay(self.selectedDay)
        }
        else if delegate != nil
        {
            self.delegate.failedToLoadDataWithError(NSError(domain: "Unable to load Data", code: 1020, userInfo: nil))
        }
    }
    
    func didLoadLocationsDataWithData(dataList: Array<LocationEntity>!) {
        
        if self.streakDataList != nil && !streakDataList.isEmpty
        {
            for location in dataList
            {
                for streak in self.streakDataList
                {
                    if location.arrivedAt.integerValue >= streak.start.integerValue && location.arrivedAt.integerValue  <= streak.end.integerValue
                    {
                        streak.location = location
                        
                        break
                    }
                }
            }
            
            self.dayStreakDataModel.streaks = self.streakDataList
            
            if self.delegate != nil
            {
                self.delegate.didLoadDataWithResult(self.dayStreakDataModel)
            }
        }
        else if delegate != nil
        {
            self.delegate.failedToLoadDataWithError(NSError(domain: "Unable to load Data", code: 1020, userInfo: nil))
        }
    }
    
    
    
    func failedToLoadStrakDataWithError(error: NSError?) {
        
        if delegate != nil
        {
            self.delegate.failedToLoadDataWithError(error)
        }
        
    }
    
    func failedToLoadPhotosDataWithError(error: NSError?) {
        
        if delegate != nil
        {
            self.delegate.failedToLoadDataWithError(error)
        }
    }
    
    func failedToLoadLocationsDataWithError(error: NSError?) {
        
        if delegate != nil
        {
            self.delegate.failedToLoadDataWithError(error)
        }
    }
}
