//
//  DateUtils.swift
//
//  Created by Ahmed Zaytoun on 4/11/16.
//  Copyright © 2016 iPalApps. All rights reserved.
//

import UIKit

enum SystemFormats
{
    case ISO
    case UTC
    case DATE_ONLY
    case HOURS_ONLY
    case SESSION
    case LONG_DATE_ONLY
}

public extension NSDate {
    
    public class func ISOStringFromDate(date: NSDate) -> String {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.timeZone = NSTimeZone(abbreviation: "GMT")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        return dateFormatter.stringFromDate(date).stringByAppendingString("Z")
    }
    
    public class func dateFromISOString(string: String) -> NSDate? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        return dateFormatter.dateFromString(string)
    }
}

class DateUtils: NSObject {

    class func getStringDate(date : NSDate!,forSystemFormat format : SystemFormats ) -> String!
    {
        if date == nil
        {
            return nil
        }
        
        var dateFormat = "yyyy-MM-dd"
        
        let dateFormatter = NSDateFormatter()
        
        switch format
        {
            case .ISO:
                return NSDate.ISOStringFromDate(date)
            
            case .UTC:
                dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            case .DATE_ONLY:
            
                break
            case .HOURS_ONLY:
                
                dateFormat = "HH:mm"
            case .SESSION:
                dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            case .LONG_DATE_ONLY:
            
                dateFormat = "MMM dd, yyyy"
        }
        
        dateFormatter.dateFormat = dateFormat
        
        return dateFormatter.stringFromDate(date)
    }
    
    
    class func getDateString(date : String!,forSystemFormat format : SystemFormats ) -> NSDate?
    {
        if date == nil
        {
            return nil
        }
        
        var dateFormat = "yyyy-MM-dd"
        
        let dateFormatter = NSDateFormatter()
        
        switch format
        {
        case .ISO:
            return NSDate.dateFromISOString(date)
            
        case .UTC:
            dateFormat = "yyyy-MM-dd'T'HH:mm:ss"

        case .DATE_ONLY:
            
            break
        case .HOURS_ONLY:
            dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        case .SESSION:
            dateFormat = "yyyy-MM-dd HH:mm:ss"
        case .LONG_DATE_ONLY:
            
            dateFormat = "MMM dd, yyyy"
        }
        
        dateFormatter.dateFormat = dateFormat
        
        return dateFormatter.dateFromString(date)
    }


    class func getDateTimeZero(date : NSDate) -> NSDate
    {
        let cal = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        
        let newDate = cal.startOfDayForDate(date)
        
        return newDate
    }
    
}
