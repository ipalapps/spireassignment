

import UIKit

struct LoggerTagNames
{
    static let DEBUG = "DEBUG"
    static let INFO = "INFO"
    static let ERROR = "ERROR"
}

class Logger: NSObject {
   
    class func debug(message : String!)
    {
        self.logMessage(message, withTag: LoggerTagNames.DEBUG)
    }
    
    class func info(message : String!)
    {
        self.logMessage(message, withTag: LoggerTagNames.INFO)
    }
    
    class func error(message : String!, withError error : NSError!)
    {
        self.logMessage(message, withTag: LoggerTagNames.ERROR)
    }
    
    private class func logMessage(message : String!, withTag tag : String!)
    {
        print("\(tag) : \(message)")
    }

}
