//
//  PhotoEntity.swift
//  strikes
//
//  Created by Ahmed Zaytoun on 6/25/16.
//  Copyright © 2016 spier. All rights reserved.
//

import UIKit

class PhotoEntity: NSObject {

    var taken : NSNumber!
    var url : String!
    
}
