//
//  StreakEntity.swift
//  strikes
//
//  Created by Ahmed Zaytoun on 6/25/16.
//  Copyright © 2016 spier. All rights reserved.
//

import UIKit

class StreakEntity: NSObject {

    var start : NSNumber!
    var end : NSNumber!
    var type : String!
    
    var startTime : String!
    var duration : Int!
    
    var location : LocationEntity?
    var photo : PhotoEntity?
}
