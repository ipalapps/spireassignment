//
//  StreakDataParser.swift
//  strikes
//
//  Created by Ahmed Zaytoun on 6/25/16.
//  Copyright © 2016 spier. All rights reserved.
//

import UIKit

class StreakDataParser: BaseParser {

    func parserStreaksList(jsonObject : AnyObject?) -> Array<StreakEntity>!
    {
        if self.isValidJsonObject(jsonObject) && jsonObject is NSArray
        {
            var dataArray = Array<StreakEntity>()
            
            for item in jsonObject as! NSArray
            {
                let streak = self.parseStreakData(item)
                
                if streak != nil
                {
                    dataArray.append(streak)
                }
            }
            
            return dataArray
        }
        
        return nil
    }
    
    func parseStreakData(jsonObject : AnyObject?) -> StreakEntity!
    {
        if self.isValidJsonObject(jsonObject) && jsonObject is NSDictionary
        {
            let streak = StreakEntity()
            
            streak.type = self.getStringDataFromObject(jsonObject, forKey: "type")
            streak.start = self.getNumberDataFromObject(jsonObject, forKey: "start_at")
            streak.end = self.getNumberDataFromObject(jsonObject, forKey: "stop_at")
            
            let date = NSDate(timeIntervalSince1970: NSTimeInterval(streak.start.doubleValue))
            
            streak.startTime = DateUtils.getStringDate(date, forSystemFormat: SystemFormats.HOURS_ONLY)
            streak.duration = (streak.end.integerValue - streak.start.integerValue) / 60
            
            return streak
        }
        
        return nil
    }
    
}
