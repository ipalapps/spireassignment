//
//  DayStreakEnitiy.swift
//  strikes
//
//  Created by Ahmed Zaytoun on 6/26/16.
//  Copyright © 2016 spier. All rights reserved.
//

import UIKit

class DayStreakEntity: NSObject {

    var date : NSDate!
    var day : Double!
    var stringDate : String!
    var streaks : Array<StreakEntity>!
    
}
