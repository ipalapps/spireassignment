
import UIKit

enum DataServices{
    
    case STREAKS
    case LOCATIONS
    case PHOTOS
};

struct ServiceStatusResponse
{
    static let SUCCESS : Int = 200
    static let ERROR_600 : Int = 600
    static let UNAUTHERIZED  : Int = 401
    static let FAILURE  : Int = 800
}
    
struct REQUEST_VALUES {
    static let httpPart : String = "http://"
    static let serverDNS : String = "spire-challenge.herokuapp.com"
    static let serverPath : String = "spire-challenge.herokuapp.com"
    static let serverPort : String = ""
    static let servicePart : String = ""
    static let formdataBoundary : String = "----WebKitFormBoundaryE19zNvXGzXaLvS5C"
}

struct REQUEST_METHOD {
    static let GET : String = "GET"
    static let PUT : String = "PUT"
    static let POST : String = "POST"
    static let DELETE : String = "DELETE"
}

struct SERVICE_URL
{
    static let locations = "/locations/"
    static let photos = "/photos/"
    static let streaks = "/streaks/"
}

class RequestUtil: NSObject {

    class func getRrequestForService(service : DataServices, body : NSData!, param : String! , options : Dictionary<String,String>!) -> NSURLRequest!
    {
        
        let stringURL = getServiceURLString(service, withParam: param)
        
        let escapedStringURL = stringURL.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        
        Logger.info("URL: \(stringURL)\nEscapedURL:\(escapedStringURL!)")
        
        let requestMethod : String  = getServiceMethod(service)
        
        let url = NSURL(string: escapedStringURL!)
        
        if url != nil
        {
            let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
            
            if options != nil
            {
                for (key,value) in options
                {
                    Logger.debug(
                    "\(key) \(value)")
                    request.addValue(value, forHTTPHeaderField: key)
                }
            }
            
            request.HTTPBody = body
            request.HTTPMethod = requestMethod
            
            return request;
        }

        return nil
    }
    
    class func getServiceURLString(service :DataServices,withParam param : String! ) -> String
    {
        
        let serverURL : String = getServerBaseURL()
        
        var urlString : String = "\(serverURL)"
        
        switch service
        {
        case .STREAKS:
                urlString += "\(SERVICE_URL.streaks)\(param != nil ? param : "")"
        case .PHOTOS:
            urlString += "\(SERVICE_URL.photos)\(param != nil ? param : "")"
            
        case .LOCATIONS:
            urlString += "\(SERVICE_URL.locations)\(param != nil ? param : "")"
        }
        
        return urlString;
    }
    
    class func getServiceMethod(service : DataServices) -> String
    {
        switch service
        {
        
            case .STREAKS,.LOCATIONS,.PHOTOS:
                return REQUEST_METHOD.GET
        }
    }
    
    class func getUTF8DataFromString(str : String) -> NSData?
    {
        return str.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
    }
    
    class func getServerBaseURL () -> String
    {
        return "\(REQUEST_VALUES.httpPart)\(REQUEST_VALUES.serverPath)\(REQUEST_VALUES.serverPort)\(REQUEST_VALUES.servicePart)"
    }
    
    class  func getJsonRequestOptions() -> Dictionary<String,String>
    {
        
        var dictioary = Dictionary<String,String>()
        
        dictioary["Accept"] = "application/json"
        
        dictioary["Content-Type"] = "application/json"
        
        return dictioary
    }
}
