//
//  StreakOperations.swift
//  strikes
//
//  Created by Ahmed Zaytoun on 6/25/16.
//  Copyright © 2016 spier. All rights reserved.
//

import UIKit

protocol LocationsOperationDelegate : NSObjectProtocol {
    
    func didLoadLocationsDataWithData(dataList : Array<LocationEntity>!)
    func failedToLoadLocationsDataWithError(error : NSError?)
}

class LocationsOperation: NSObject, ServiceManagerDelegate {

    var serviceManager : ServiceManager!
    
    lazy var locationsParser : LocationsParser! = LocationsParser()
    
    weak var delegate : LocationsOperationDelegate!
    
    override init()
    {
        super.init()
        
        self.serviceManager = ServiceManager()
        
        self.serviceManager.delegate = self
    }
    
    func loadPhotosDataForDay(day : NSNumber)
    {
        self.serviceManager.getLocationsForDay(day)
    }
    
    func didLoadData(data: AnyObject?, withResponse response: NSURLResponse?) {
        
        let dataList = self.locationsParser.paserLocationsList(data)
        
        if self.delegate != nil
        {
            self.delegate.didLoadLocationsDataWithData(dataList)
        }
    }
    
    func failedToLoadDataWithError(error: NSError?, withResponse response: NSURLResponse?) {
        
        if self.delegate != nil
        {
            self.delegate.failedToLoadLocationsDataWithError(error)
        }
        
    }
    
}
