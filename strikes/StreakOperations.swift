//
//  StreakOperations.swift
//  strikes
//
//  Created by Ahmed Zaytoun on 6/25/16.
//  Copyright © 2016 spier. All rights reserved.
//

import UIKit

protocol StreakOperationDelegate : NSObjectProtocol {
    
    func didLoadStrakDataWithData(dataList : Array<StreakEntity>!)
    func failedToLoadStrakDataWithError(error : NSError?)
}

class StreakOperations: NSObject, ServiceManagerDelegate {

    var serviceManager : ServiceManager!
    
    lazy var streakParser : StreakDataParser! = StreakDataParser()
    
    weak var delegate : StreakOperationDelegate!
    
    override init()
    {
        super.init()
        
        self.serviceManager = ServiceManager()
        
        self.serviceManager.delegate = self
    }
    
    func loadStreaksDataForDay(day : NSNumber)
    {
        self.serviceManager.getStreaksForDay(day)
        
//        let streakOper :  NSBlockOperation = NSBlockOperation(block: {
//            Logger.debug("Streaks")
//            self.serviceManager.getStreaksForDay(day)
//        })
//        
//        let photoOper : NSBlockOperation = NSBlockOperation(block: {
//            Logger.debug("Photos")
//            self.serviceManager.getPhotosForDay(day)
//        })
//        
//        let locationsOper : NSBlockOperation = NSBlockOperation(block: {
//            Logger.debug("Locations")
//            self.serviceManager.getLocationsForDay(day)
//        })
//        
//        photoOper.addDependency(streakOper)
//        photoOper.addDependency(locationsOper)
//
//        self.operationQueue.addOperation(locationsOper)
//        self.operationQueue.addOperation(photoOper)
//        self.operationQueue.addOperation(streakOper)
        
        
    }
    
    func didLoadData(data: AnyObject?, withResponse response: NSURLResponse?) {
        
        let dataList = self.streakParser.parserStreaksList(data)
        
        if self.delegate != nil
        {
            self.delegate.didLoadStrakDataWithData(dataList)
        }
    }
    
    func failedToLoadDataWithError(error: NSError?, withResponse response: NSURLResponse?) {
        
        if self.delegate != nil
        {
            self.delegate.failedToLoadStrakDataWithError(error)
        }
        
    }
    
}
