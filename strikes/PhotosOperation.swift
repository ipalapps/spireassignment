//
//  StreakOperations.swift
//  strikes
//
//  Created by Ahmed Zaytoun on 6/25/16.
//  Copyright © 2016 spier. All rights reserved.
//

import UIKit

protocol PhotosOperationDelegate : NSObjectProtocol {
    
    func didLoadPhotosDataWithData(dataList : Array<PhotoEntity>!)
    func failedToLoadPhotosDataWithError(error : NSError?)
}

class PhotosOperation: NSObject, ServiceManagerDelegate {

    var serviceManager : ServiceManager!
    
    lazy var photosParser : PhotoDataParser! = PhotoDataParser()
    
    weak var delegate : PhotosOperationDelegate!
    
    override init()
    {
        super.init()
        
        self.serviceManager = ServiceManager()
        
        self.serviceManager.delegate = self
    }
    
    func loadPhotosDataForDay(day : NSNumber)
    {
        self.serviceManager.getPhotosForDay(day)
    }
    
    func didLoadData(data: AnyObject?, withResponse response: NSURLResponse?) {
        
        let dataList = self.photosParser.paserPhotosList(data)
        
        if self.delegate != nil
        {
            self.delegate.didLoadPhotosDataWithData(dataList)
        }
    }
    
    func failedToLoadDataWithError(error: NSError?, withResponse response: NSURLResponse?) {
        
        if self.delegate != nil
        {
            self.delegate.failedToLoadPhotosDataWithError(error)
        }
        
    }
    
}
