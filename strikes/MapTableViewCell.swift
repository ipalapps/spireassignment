//
//  MapTableViewCell.swift
//  strikes
//
//  Created by Ahmed Zaytoun on 6/26/16.
//  Copyright © 2016 spier. All rights reserved.
//

import UIKit
import MapKit

class MapTableViewCell: LabelsTableViewCell {

    @IBOutlet weak var mapView : MKMapView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
