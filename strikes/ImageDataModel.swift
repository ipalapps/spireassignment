//
//  ImageDataModel.swift
//
//
//  Created by Ahmed Zaytoun on 3/13/16.
//  Copyright © 2016 Harri. All rights reserved.
//

import UIKit

enum ImageStatus
{
    case DOWNLOADING
    case DOWNLOADED
    case FAILED_TO_DOWNLOAD
}

class ImageDataModel: NSObject {

    var identifier : String!
    var status : ImageStatus!
    var image : UIImage!

}
