//
//  ImageServiceManager.swift
//  harriEmployer
//
//  Created by Ahmed Zaytoun on 3/12/16.
//  Copyright © 2016 Harri. All rights reserved.
//

import UIKit

enum ImageCaches
{
    case ALL
}

struct ImageServiceManagerConstants
{
    static let DID_FINISH_DOWNLOAD  = "didFinishDownload"
}

class ImageServiceManager: NSObject,NSURLSessionDelegate,NSURLSessionDownloadDelegate {

    private var imageCache : Dictionary<String,ImageDataModel> = Dictionary<String,ImageDataModel>()
    
    static private var singletoneManager : ImageServiceManager!
    
    private var session : NSURLSession!
    
    private override init() {
        
        super.init()
        
        let configiration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        configiration.allowsCellularAccess = true
        configiration.timeoutIntervalForRequest = 30.0
        configiration.timeoutIntervalForResource = 60.0
        
        self.session = NSURLSession(configuration: configiration, delegate: self, delegateQueue: nil)
    }
    
    func getImageForIdentifier(identifier : String!,forURL url : NSURL!, fromCache cache : ImageCaches) -> ImageDataModel!
    {
        if identifier == nil || identifier.isEmpty
        {
            return nil
        }
        
        switch cache
        {
          
            
            case .ALL:
                
                let image = self.imageCache[identifier]
                    
                if image != nil
                {
                    return image
                }
        }
        
        if url != nil
        {
            self.downloadImageWithIdentifier(identifier, forURL: url, forCache: cache)
        }
        
        return nil
    }
    
    
    func downloadImageWithIdentifier(identifier : String, forURL url : NSURL, forCache cache : ImageCaches) -> ImageDataModel!
    {
     
        Logger.debug("Downloading image : \(url.description)")
        self.addImageToCache(identifier, imageData: nil, toCache: cache, withStatus:ImageStatus.DOWNLOADING)
        
        let downloadTask : NSURLSessionDownloadTask = self.session.downloadTaskWithURL(url, completionHandler:
            {
                (resURL : NSURL?, response : NSURLResponse?, error : NSError?) in
                
                if error == nil && resURL != nil
                {
                    
                    let imageData = NSData(contentsOfURL: resURL!)

                    if imageData != nil
                    {
                        let image = UIImage(data: imageData!)
                        
                        if image != nil
                        {
                            self.addImageToCache(identifier, imageData: image, toCache: cache, withStatus:ImageStatus.DOWNLOADED)
                            NSNotificationCenter.defaultCenter().postNotificationName(ImageServiceManagerConstants.DID_FINISH_DOWNLOAD, object: nil)
                            return
                        }
                        
                    }
                    
                    self.addImageToCache(identifier, imageData: nil, toCache: cache, withStatus:ImageStatus.FAILED_TO_DOWNLOAD)
                    NSNotificationCenter.defaultCenter().postNotificationName(ImageServiceManagerConstants.DID_FINISH_DOWNLOAD, object: nil)
                }
                
            })
        
        
        downloadTask.resume()
        
        return nil
    }
    
    
    static func getImageDownloaderManagerInstance() -> ImageServiceManager
    {
        
        if self.singletoneManager == nil
        {
            self.singletoneManager = ImageServiceManager()
        }
        return self.singletoneManager
    }
    
    func addImageToCache(identifier : String, imageData img : UIImage!, toCache cache : ImageCaches, withStatus status : ImageStatus)
    {
     
        
        let imageModel =  ImageDataModel()
        
        imageModel.identifier = identifier
        imageModel.image = img
        imageModel.status = status

        switch cache
        {
            case .ALL:
            
                self.imageCache[identifier] = imageModel
        }
        
    }
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didFinishDownloadingToURL location: NSURL) {
        
        
        
    }
    
}
