//
//  LocationEntity.swift
//  strikes
//
//  Created by Ahmed Zaytoun on 6/25/16.
//  Copyright © 2016 spier. All rights reserved.
//

import UIKit

class LocationEntity: NSObject {

    var arrivedAt : NSNumber!
    var latitude : NSNumber!
    var longitude : NSNumber!
}
